function toggleMenu() {
    const menuButton = document.getElementById("menu-button");
    const menuIconOpen = document.getElementById("menu-icon-open");
    const menuIconClose = document.getElementById("menu-icon-close");
    const menu = document.getElementById("menu");

    menuButton.addEventListener('click', function () {
        menuIconOpen.classList.toggle("display-none");
        menuIconClose.classList.toggle("display-none");
        menu.classList.toggle("show");
    });
}