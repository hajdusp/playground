const skills = [
    {
        name: "HTML",
        startYear: 2016
    },
    {
        name: "CSS",
        startYear: 2016
    },
    {
        name: "Javascript",
        startYear: 2017
    },
    {
        name: "Git",
        startYear: 2018
    },
    {
        name: "WordPress",
        startYear: 2020
    },
    {
        name: "Figma",
        startYear: 2021
    },
    {
        name: "Angular",
        startYear: 2021
    },
];

const currentYear = new Date().getFullYear();

// find the max startYear to calculate the maximum years of experience - the progress bar widths will be calculated according to this
// https://stackoverflow.com/questions/4020796/finding-the-max-value-of-an-attribute-in-an-array-of-objects
const minStartYear = Math.min(...skills.map(o => o.startYear));

const maxExperience = currentYear - minStartYear;

function addSkills() {

    const skillsParent = document.getElementById('skills');

    for (let skill of skills) {

        const currentExperience = currentYear - skill.startYear;

        skillsParent.innerHTML += `
        <div class="skill">
            <div class="skill-name">${skill.name}</div>
                <div class="skill-experience">
                    <div class="skill-progress">
                    <div class="skill-progress-bar" style="width: ${currentExperience / maxExperience * 100}%"></div>
</div>
                    <div class="skill-years">${currentExperience} ${(currentExperience === 1) ? 'year' : 'years'}</div>
                </div>
            </div>
        `
    }
}

addSkills();
